/*
 * 
 */
package cz.utb.fai.crypto.benchmark.symmetric;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.Security;
import java.util.concurrent.TimeUnit;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;

/**
 * AES JCE benchmarker.
 *
 * @author František Špaček
 */
@Fork(3)
@State(Scope.Thread)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.MILLISECONDS)
public class BcAesBenchmarker extends BaseSymetricCryptoBechmarker {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    @Setup(Level.Iteration)
    @Override
    public void setup() throws NoSuchAlgorithmException,
            NoSuchPaddingException {
        setupRandomOpenText();
        setupKey("AES");
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding", "BC");
        } catch (NoSuchProviderException ex) {
            System.out.println(ex);
        }
    }

    @Benchmark
    @Override
    public byte[] encrypt1MiB() throws InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException {
        return super.encrypt1MiB();
    }

    @Benchmark
    @Override
    public byte[] encrypt5MiB() throws InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException {
        return super.encrypt5MiB();
    }

    @Benchmark
    @Override
    public byte[] encrypt10MiB() throws InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException {
        return super.encrypt10MiB();
    }
}
