/*
 * 
 */
package cz.utb.fai.crypto.benchmark.symmetric;

import cz.utb.fai.crypto.benchmark.CryptoBenchmarker;
import java.security.InvalidKeyException;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

/**
 * Defines benchmark for symmetric ciphers.
 *
 * @author František Špačeks
 */
public interface SymmetricCryptoBenchmarker extends CryptoBenchmarker {

    byte[] encrypt10MiB() throws InvalidKeyException, IllegalBlockSizeException,
            BadPaddingException;

    byte[] encrypt1MiB() throws InvalidKeyException, IllegalBlockSizeException,
            BadPaddingException;

    byte[] encrypt5MiB() throws InvalidKeyException, IllegalBlockSizeException,
            BadPaddingException;

}
