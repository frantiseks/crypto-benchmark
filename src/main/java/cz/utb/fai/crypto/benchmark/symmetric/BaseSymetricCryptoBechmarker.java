/*
 * 
 */
package cz.utb.fai.crypto.benchmark.symmetric;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

/**
 * Base symmetric crypto benchmarker implementation of
 * {@link SymmetricCryptoBenchmarker}.
 *
 * @author František Špaček
 */
public abstract class BaseSymetricCryptoBechmarker
        implements SymmetricCryptoBenchmarker {

    private static final int KEY_SIZE = 256;
    protected static final SecureRandom RANDOM = new SecureRandom();
    protected SecretKey key;
    protected final byte[] openText1MiB = new byte[1024 * 1024];
    protected final byte[] openText5MiB = new byte[1024 * 1024 * 5];
    protected final byte[] openText10MiB = new byte[1024 * 1024 * 10];
    protected Cipher cipher;

    @Override
    public byte[] encrypt1MiB() throws InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException {
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return cipher.doFinal(openText1MiB);
    }

    @Override
    public byte[] encrypt5MiB() throws InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException {
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return cipher.doFinal(openText5MiB);
    }

    @Override
    public byte[] encrypt10MiB() throws InvalidKeyException,
            IllegalBlockSizeException, BadPaddingException {
        cipher.init(Cipher.ENCRYPT_MODE, key);
        return cipher.doFinal(openText10MiB);
    }

    protected void setupKey(String keyGeneratorName)
            throws NoSuchAlgorithmException {
        KeyGenerator kgen = KeyGenerator.getInstance(keyGeneratorName);
        kgen.init(KEY_SIZE);
        key = kgen.generateKey();
    }

    protected void setupRandomOpenText() {
        RANDOM.nextBytes(openText1MiB);
        RANDOM.nextBytes(openText5MiB);
        RANDOM.nextBytes(openText10MiB);
    }
}
