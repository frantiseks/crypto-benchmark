/*
 */
package cz.utb.fai.crypto.benchmark.asymmetric;

import cz.utb.fai.crypto.benchmark.CryptoBenchmarker;

/**
 * Defines benchmarker methods for asymmetric cryptographic algorithms.
 *
 * @author František Špaček
 */
public interface AsymmetricCryptoBenchmarker extends CryptoBenchmarker {

}
