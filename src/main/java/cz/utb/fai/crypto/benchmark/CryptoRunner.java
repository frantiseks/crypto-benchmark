/*
 */
package cz.utb.fai.crypto.benchmark;

import java.io.IOException;
import org.openjdk.jmh.Main;
import org.openjdk.jmh.runner.RunnerException;

/**
 * Runner for crypto benchmarkers.
 *
 * @author František Špaček
 */
public class CryptoRunner {

    public static void main(String... args) 
            throws RunnerException, IOException {
        Main.main(args);
    }
}
