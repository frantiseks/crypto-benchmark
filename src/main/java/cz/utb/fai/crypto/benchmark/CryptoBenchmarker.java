/*
 * 
 */
package cz.utb.fai.crypto.benchmark;

import java.security.NoSuchAlgorithmException;
import javax.crypto.NoSuchPaddingException;

/**
 * Defines common benchmarker methods.
 *
 * @author František Špaček
 */
public interface CryptoBenchmarker {

    void setup() throws NoSuchAlgorithmException, NoSuchPaddingException;
}
